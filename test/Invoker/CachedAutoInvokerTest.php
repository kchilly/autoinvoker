<?php

declare(strict_types=1);


namespace Csoft\AutoInvokerTest\Invoker;


use Csoft\AutoInvoker\AutoInvokeRule\AutoRegisterAllRule;
use Csoft\AutoInvoker\AutoInvokeRule\AutoRegisterRule;
use Csoft\AutoInvoker\ClassFinder\ClassFinder;
use Csoft\AutoInvoker\Invoker\CachedAutoInvoker;
use Csoft\AutoInvokerTest\CacheImplementation\TestableApcuCacheImplementation;
use Csoft\AutoInvokerTest\CacheImplementation\TestableFileSystemCacheImplementation;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Filesystem;

class CachedAutoInvokerTest extends TestCase
{
    public function testFileSystemCacheImplementationFullCycle()
    {
        ob_start();
        $cachePath = __DIR__ . '/../../var/cache';
        (new Filesystem())->remove($cachePath);
        $cacheImplementation = new TestableFileSystemCacheImplementation($cachePath);
        $cacheImplementation->resetCache();

        $autoInvoker = new CachedAutoInvoker(
            new ClassFinder(),
            $cacheImplementation
        );
        $autoInvoker->addInvokeRule(new AutoRegisterRule([__DIR__ . '/../Fixture']));
        $autoInvoker->invoke();
        $autoInvoker->invoke();

        $cacheImplementation->resetCache();

        // checking cache, empty
        // fetching from file and shows
        // storing into cache
        // fetching from cache
        $expected = 'The cache is empty!AutoRegister2AutoRegister1Cache written!AutoRegister2AutoRegister1';

        $this->assertEquals($expected, ob_get_clean());
    }

    public function testApcuCacheImplementationFullCycle()
    {
        ob_start();
        $cacheImplementation = new TestableApcuCacheImplementation();
        $cacheImplementation->resetCache();

        $autoInvoker = new CachedAutoInvoker(
            new ClassFinder(),
            $cacheImplementation
        );
        $autoInvoker->addInvokeRule(new AutoRegisterRule([__DIR__ . '/../Fixture']));
        $autoInvoker->invoke();
        $autoInvoker->invoke();

        $cacheImplementation->resetCache();

        // checking cache, empty
        // fetching from file and shows
        // storing into cache
        // fetching from cache
        $expected = 'The cache is empty!AutoRegister2AutoRegister1Cache written!AutoRegister2AutoRegister1';

        $this->assertEquals($expected, ob_get_clean());
    }

    public function testFileSystemCacheImplementationFullCycleAllClass()
    {
        ob_start();
        $cachePath = __DIR__ . '/../../var/cache';
        (new Filesystem())->remove($cachePath);
        $cacheImplementation = new TestableFileSystemCacheImplementation($cachePath);
        $cacheImplementation->resetCache();

        $autoInvoker = new CachedAutoInvoker(
            new ClassFinder(),
            $cacheImplementation
        );
        $autoInvoker->addInvokeRule(new AutoRegisterAllRule([__DIR__ . '/../Fixture']));
        $autoInvoker->invoke();
        $autoInvoker->invoke();

        $cacheImplementation->resetCache();

        // checking cache, empty
        // fetching from file and shows
        // storing into cache
        // fetching from cache
        $expected = 'The cache is empty!FirstClassSecondClassMultipleNameSpacedClassesAutoRegister2AutoRegister1Cache written!FirstClassSecondClassMultipleNameSpacedClassesAutoRegister2AutoRegister1';

        $this->assertEquals($expected, ob_get_clean());
    }

    public function testApcuCacheImplementationFullCycleAllClass()
    {
        ob_start();
        $cacheImplementation = new TestableApcuCacheImplementation();
        $cacheImplementation->resetCache();

        $autoInvoker = new CachedAutoInvoker(
            new ClassFinder(),
            $cacheImplementation
        );
        $autoInvoker->addInvokeRule(new AutoRegisterAllRule([__DIR__ . '/../Fixture']));
        $autoInvoker->invoke();
        $autoInvoker->invoke();

        $cacheImplementation->resetCache();

        // checking cache, empty
        // fetching from file and shows
        // storing into cache
        // fetching from cache
        $expected = 'The cache is empty!FirstClassSecondClassMultipleNameSpacedClassesAutoRegister2AutoRegister1Cache written!FirstClassSecondClassMultipleNameSpacedClassesAutoRegister2AutoRegister1';

        $this->assertEquals($expected, ob_get_clean());
    }
}
