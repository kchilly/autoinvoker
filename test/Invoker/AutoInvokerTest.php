<?php

declare(strict_types=1);


namespace Csoft\AutoInvokerTest\Invoker;


use Csoft\AutoInvoker\AutoInvokeRule\AutoRegisterRule;
use Csoft\AutoInvoker\Invoker\AutoInvoker;
use Csoft\AutoInvokerTest\ClassFinder\DummyClassFinder;
use PHPUnit\Framework\TestCase;

class AutoInvokerTest extends TestCase
{
    public function testDummyInvoke()
    {
        ob_start();
        $autoInvoker = new AutoInvoker(new DummyClassFinder());
        $autoInvoker->addInvokeRule(new AutoRegisterRule([__DIR__ . '../Fixture']));
        $autoInvoker->invoke();

        $expected = 'AutoRegister1AutoRegister2';

        $this->assertEquals($expected, ob_get_clean());
    }
}
