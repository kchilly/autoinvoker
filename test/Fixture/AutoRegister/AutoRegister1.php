<?php

declare(strict_types=1);


namespace Csoft\AutoInvokerTest\Fixture\AutoRegister;


use Csoft\AutoInvoker\InvokableInterface\AutoRegisterInterface;

class AutoRegister1 implements AutoRegisterInterface
{
    public static function register()
    {
        echo 'AutoRegister1';
    }
}
