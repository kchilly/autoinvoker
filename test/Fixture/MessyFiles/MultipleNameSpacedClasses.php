<?php

declare(strict_types=1);

namespace Csoft\AutoInvokerTest\Fixture\MessyFiles;


class FirstClass
{
    public static function register()
    {
        echo 'FirstClass';
    }
}

class SecondClass
{
    public static function register()
    {
        echo 'SecondClass';
    }
}

abstract class AbstractClass
{
    public static function register()
    {
        echo 'AbstractClass';
    }
}

class MultipleNameSpacedClasses
{
    public static function register()
    {
        echo 'MultipleNameSpacedClasses';
    }
}

