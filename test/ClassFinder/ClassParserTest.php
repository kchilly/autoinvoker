<?php

declare(strict_types=1);


namespace Csoft\AutoInvokerTest\ClassFinder;


use Csoft\AutoInvoker\ClassFinder\ClassParser;
use PHPUnit\Framework\TestCase;

class ClassParserTest extends TestCase
{
    /**
     * @param string $expected
     * @param string $content
     *
     * @dataProvider provideForGetNameSpaceFromContent
     */
    public function testGetNameSpaceFromContent(string $expected, string $content)
    {
        $this->assertEquals(
            $expected,
            ClassParser::getNameSpaceFromContent($content)
        );
    }

    public function provideForGetNameSpaceFromContent(): array
    {
        return [
            [
                'Csoft\AutoInvoker',
                <<<EOD
    
        namespace Csoft\AutoInvoker;

EOD,
            ],
            [
                'Csoft\AutoInvoker',
                <<<EOD
    
        NameSpace Csoft\AutoInvoker;

EOD,
            ],
            [
                '',
                <<<EOD
    
        class AutoInvoker {
        
        }

EOD,
            ],
        ];
    }

    /**
     * @param string $expected
     * @param string $content
     *
     * @dataProvider provideForGetClassNameFromContent
     */
    public function testGetClassNameFromContent(array $expected, string $content)
    {
        $this->assertEquals(
            $expected,
            ClassParser::getClassNameFromContent($content)
        );
    }

    public function provideForGetClassNameFromContent(): array
    {
        return [
            [
                ['AutoInvoker'],
                <<<EOD
    
        class AutoInvoker {
        }

EOD,
            ],
            [
                [],
                <<<EOD
    
        function autoInvoke() {
        }

EOD,
            ],
            [
                [
                    'AutoInvoker',
                    'AutoInvoker2',
                ],
                <<<EOD
    
        class AutoInvoker
        {
        
        }

        class AutoInvoker2 {
        
        }

EOD,
            ],
        ];
    }
}
