<?php

declare(strict_types=1);


namespace Csoft\AutoInvokerTest\ClassFinder;


use Csoft\AutoInvoker\AutoInvokeRule\AutoInvokeRuleInterface;
use Csoft\AutoInvoker\ClassFinder\ClassFinder;

class TestableClassFinder extends ClassFinder
{
    public function fetchClasses(AutoInvokeRuleInterface $rule)
    {
        parent::fetchClasses($rule);
    }

    public function getClasses(): array
    {
        return $this->classes;
    }
}
