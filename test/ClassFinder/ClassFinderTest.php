<?php

declare(strict_types=1);


namespace Csoft\AutoInvokerTest\ClassFinder;


use Csoft\AutoInvoker\AutoInvokeRule\AutoRegisterRule;
use Csoft\AutoInvoker\ClassFinder\ClassFinder;
use PHPUnit\Framework\TestCase;

class ClassFinderTest extends TestCase
{
    public function testGetClasses()
    {
        $classFinder = new TestableClassFinder();
        $classFinder->fetchClasses(
            new AutoRegisterRule([__DIR__ . '/../Fixture'])
        );

        $expected = [
            __DIR__ . '/../Fixture' => [
                'Csoft\AutoInvokerTest\Fixture\AutoRegister\AutoRegister2' => [
                    'Csoft\AutoInvoker\InvokableInterface\AutoRegisterInterface',
                ],
                'Csoft\AutoInvokerTest\Fixture\AutoRegister\AutoRegister1' => [
                    'Csoft\AutoInvoker\InvokableInterface\AutoRegisterInterface',
                ],
                'Csoft\AutoInvokerTest\Fixture\MessyFiles\MultipleNameSpacedClasses' => [],
            ],
        ];

        $this->assertEquals($expected, $classFinder->getClasses());
    }

    public function testGetMatchingClasses()
    {
        $classFinder = new ClassFinder();

        $expected = [
            'Csoft\AutoInvokerTest\Fixture\AutoRegister\AutoRegister2',
            'Csoft\AutoInvokerTest\Fixture\AutoRegister\AutoRegister1',
        ];

        $this->assertEquals(
            $expected,
            $classFinder->getMatchingClasses(
                new AutoRegisterRule([__DIR__ . '/../Fixture'])
            )
        );
    }
}
