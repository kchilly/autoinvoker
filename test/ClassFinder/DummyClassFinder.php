<?php

declare(strict_types=1);


namespace Csoft\AutoInvokerTest\ClassFinder;


use Csoft\AutoInvoker\AutoInvokeRule\AutoInvokeRuleInterface;
use Csoft\AutoInvoker\ClassFinder\ClassFinderInterface;
use Csoft\AutoInvokerTest\Fixture\AutoRegister\AutoRegister1;
use Csoft\AutoInvokerTest\Fixture\AutoRegister\AutoRegister2;

class DummyClassFinder implements ClassFinderInterface
{
    public function getMatchingClasses(AutoInvokeRuleInterface $rule): array
    {
        return [
            AutoRegister1::class,
            AutoRegister2::class,
        ];
    }
}
