<?php

declare(strict_types=1);


namespace Csoft\AutoInvokerTest\CacheImplementation;


use Csoft\AutoInvoker\AutoInvokeRule\AutoInvokeRuleInterface;
use Csoft\AutoInvoker\CacheImplementation\CacheImplementationInterface;
use Csoft\AutoInvoker\CacheImplementation\FileSystemCacheImplementation;
use Csoft\AutoInvoker\NotFoundException;
use Csoft\AutoInvokerTest\Fixture\AutoRegister\AutoRegister1;
use Csoft\AutoInvokerTest\Fixture\AutoRegister\AutoRegister2;

class TestableFileSystemCacheImplementation extends FileSystemCacheImplementation
{
    public function getMatchingClasses(AutoInvokeRuleInterface $rule): array
    {
        try {
            $matchingClasses = parent::getMatchingClasses($rule);
        } catch (NotFoundException $e) {
            echo 'The cache is empty!';
            throw $e;
        }

        return $matchingClasses;
    }

    public function storeMatchingClasses(AutoInvokeRuleInterface $rule, array $matchingClasses): bool
    {
        echo 'Cache written!';

        return parent::storeMatchingClasses($rule, $matchingClasses);
    }
}
