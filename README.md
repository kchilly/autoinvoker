[![Codacy Badge](https://app.codacy.com/project/badge/Grade/970db804f8c046f7ba774da9ea4dfabe)](https://www.codacy.com/manual/kchilly86/autoinvoker?utm_source=kchilly@bitbucket.org&amp;utm_medium=referral&amp;utm_content=kchilly/autoinvoker&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://app.codacy.com/project/badge/Coverage/970db804f8c046f7ba774da9ea4dfabe)](https://www.codacy.com/manual/kchilly86/autoinvoker?utm_source=kchilly@bitbucket.org&utm_medium=referral&utm_content=kchilly/autoinvoker&utm_campaign=Badge_Coverage)
[![Latest Stable Version](https://poser.pugx.org/csoft/autoinvoker/v/stable)](https://packagist.org/packages/csoft/autoinvoker)
[![Total Downloads](https://poser.pugx.org/csoft/autoinvoker/downloads)](https://packagist.org/packages/csoft/autoinvoker)
[![License](https://poser.pugx.org/csoft/autoinvoker/license)](https://packagist.org/packages/csoft/autoinvoker)

# AutoInvoker
> AutoInvoker is a solution for invoking classes based on specific rule set. The solution only invokes static methods!

## Why was it created
> During the work with Wordpress I needed to manually register every new components in the functions.php and
now the AutoInvoker is doing the job instead of me.

## Install
```bash
composer require csoft/autoinvoker
```

## Development Usage
**!!! Always use cache layer for production !!!**  
  
Invoke the register method on every instance of the AutoRegisterInterface in the /src directory.
```php
use Csoft\AutoInvoker\Invoker\AutoInvoker;
use Csoft\AutoInvoker\ClassFinder\ClassFinder;
use Csoft\AutoInvoker\AutoInvokeRule\AutoRegisterRule;

$autoInvoker = new AutoInvoker(
    new ClassFinder()
);
$invokeRule = new AutoRegisterRule(['/src']);
$autoInvoker->addInvokeRule($invokeRule);
$autoInvoker->invoke();
```

## Production Usage
!!! There are multiple implementations, but use the APCU for better performance !!!  
  
Invoke the register method on every instance of the AutoRegisterInterface in the /src directory.
```php
use Csoft\AutoInvoker\Invoker\CachedAutoInvoker;
use Csoft\AutoInvoker\ClassFinder\ClassFinder;
use Csoft\AutoInvoker\AutoInvokeRule\AutoRegisterRule;
use Csoft\AutoInvoker\CacheImplementation\ApcuCacheImplementation;
use Csoft\AutoInvoker\CacheImplementation\FileSystemCacheImplementation;

// With apcu cache layer
$autoInvokerApcuCache = new CachedAutoInvoker(
    new ClassFinder(),
    new ApcuCacheImplementation()
);

// With file system cache layer
$autoInvokerFileCache = new CachedAutoInvoker(
    new ClassFinder(),
    new FileSystemCacheImplementation(__DIR__ . '/var/cache')
);

$invokeRule = new AutoRegisterRule([__DIR__ . '/src']);
$autoInvokerApcuCache->addInvokeRule($invokeRule);
$autoInvokerApcuCache->invoke();
```
  
---
  
## AutoInvokeRule
> These classes contain the class matching rule.

You can use the AutoRegisterRule class to identify the classes which are the implementation of the
"AutoRegisterInterface" and therefore have a register static method in the implementation. 
This register method can be invoked by the autoinvoker.  
  
To identify all classes which are implementing the AutoRegisterInterface in the /src and /src2 directory 
you can use the example below!
```php
use Csoft\AutoInvoker\AutoInvokeRule\AutoRegisterRule;

$rule = new AutoRegisterRule([
    __DIR__ . '/src',
    __DIR__ . '/src2',
]);
```
  
## InvokableInterface
> These are the classes which are enforcing the invokable method implementation in the implementation classes.  

Example for having the register static method.
```php
interface AutoRegisterInterface
{
    /**
     * The invokable method.
     *
     * @return void
     */
    public static function register();
}
```

## ClassFinder
> Class finders are the class fetching components.  

The basic implementation "ClassFinder" is looking for the classes on the filesystem under the paths defined by the AutoInvokeRule.  

## CacheImplementation
> These classes are the cache implementations to store the matching classes to avoid full filesystem parsing.

The basic implementation is using the filesystem to store the matching classes, however it's more recommended
the usage of the apcu implementation because of performance reasons.

## Invoker
> These classes are integrating the previously defined components and orchestrating their collaboration.

### AutoInvoker
It's the basic implementation which is realtime fetching the matching classes based on the given rule.
It's easy to use but only recommended in development mode.

### CachedAutoInvoker
It's the more proper implementation which is realtime fetching the matching classes based on the given rule
or loading it from a cache layer if the entries were already stored and not changed.
  
---
  
## AutoInvokeRule Customization
For having the custom rule you have two ways to go.

### Instance of
Get all the classes from the given directories which are the instance of the given interface and invoke them
with the specified method.  
  
Create the invokable interface
```php
interface CustomUnRegisterInterface
{
    public static function unRegister();
}
```
Create the invokable rule
```php
use Csoft\AutoInvoker\AutoInvokeRule\AbstractAutoInvokeRule;

class CustomUnRegisterRule extends AbstractAutoInvokeRule
{
    /**
     * @inheritDoc
     */
    public function getInvokableInterface(): string
    {
        return CustomUnRegisterInterface::class;
    }

    /**
     * @inheritDoc
     */
    public function getInvokableMethodName(): string
    {
        return 'unRegister';
    }
}
```

### All classes (not recommended)
Get all the classes from the given directories and invoke them with the specified method. 
Not recommended as you can't make sure they have the invokable method.
```php
use Csoft\AutoInvoker\AutoInvokeRule\AbstractAutoInvokeRule;

class CustomRegisterRule extends AbstractAutoInvokeRule
{
    /**
     * @inheritDoc
     */
    public function getInvokableInterface(): string
    {
        // Empty string means all classes.
        return '';
    }

    /**
     * @inheritDoc
     */
    public function getInvokableMethodName(): string
    {
        return 'register';
    }
}
```
