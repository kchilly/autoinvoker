<?php

declare(strict_types=1);


namespace Csoft\AutoInvoker\Invoker;


use Csoft\AutoInvoker\CacheImplementation\CacheImplementationInterface;
use Csoft\AutoInvoker\ClassFinder\ClassFinderInterface;
use Csoft\AutoInvoker\NotFoundException;

class CachedAutoInvoker extends AbstractAutoInvoker
{
    /** @var CacheImplementationInterface */
    private $cacheImplementation;

    /**
     * AutoInvoker constructor.
     *
     * @param ClassFinderInterface $classFinder
     * @param CacheImplementationInterface $cacheImplementation
     */
    public function __construct(ClassFinderInterface $classFinder, CacheImplementationInterface $cacheImplementation)
    {
        parent::__construct($classFinder);
        $this->cacheImplementation = $cacheImplementation;
    }

    public function invoke()
    {
        foreach ($this->rules as $rule) {
            try {
                $this->invokeAll(
                    $rule,
                    $this->cacheImplementation->getMatchingClasses($rule)
                );
            } catch (NotFoundException $e) {
                try {
                    $matchingClasses = $this->classFinder->getMatchingClasses($rule);
                    $this->invokeAll($rule, $matchingClasses);
                    $this->cacheImplementation->storeMatchingClasses($rule, $matchingClasses);
                } catch (NotFoundException $e) {
                    // there are no matches in cache neither in class finder.
                }
            }
        }
    }
}
