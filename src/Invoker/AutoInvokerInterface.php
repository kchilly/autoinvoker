<?php

declare(strict_types=1);


namespace Csoft\AutoInvoker\Invoker;


use Csoft\AutoInvoker\AutoInvokeRule\AutoInvokeRuleInterface;

interface AutoInvokerInterface
{
    /**
     * Adds a new invokable rule to the pool.
     *
     * @param AutoInvokeRuleInterface $rule
     *
     * @return void
     */
    public function addInvokeRule(AutoInvokeRuleInterface $rule);

    /**
     * Invokes all the rule matching class methods.
     *
     * @return void
     */
    public function invoke();
}
