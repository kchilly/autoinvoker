<?php

declare(strict_types=1);


namespace Csoft\AutoInvoker\Invoker;


use Csoft\AutoInvoker\NotFoundException;

class AutoInvoker extends AbstractAutoInvoker
{
    public function invoke()
    {
        foreach ($this->rules as $rule) {
            try {
                $this->invokeAll($rule, $this->classFinder->getMatchingClasses($rule));
            } catch (NotFoundException $e) {
                // there are no matches in class finder.
            }
        }
    }
}
