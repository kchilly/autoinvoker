<?php

declare(strict_types=1);


namespace Csoft\AutoInvoker\Invoker;


use Csoft\AutoInvoker\AutoInvokeRule\AutoInvokeRuleInterface;
use Csoft\AutoInvoker\ClassFinder\ClassFinderInterface;

abstract class AbstractAutoInvoker implements AutoInvokerInterface
{
    /** @var ClassFinderInterface */
    protected $classFinder;
    /** @var AutoInvokeRuleInterface[] */
    protected $rules;

    /**
     * AutoInvoker constructor.
     *
     * @param ClassFinderInterface $classFinder
     */
    public function __construct(ClassFinderInterface $classFinder)
    {
        $this->classFinder = $classFinder;
    }

    public function addInvokeRule(AutoInvokeRuleInterface $rule)
    {
        $this->rules[] = $rule;
    }

    /**
     * Invokes all the invokable classes in the list.
     *
     * @param AutoInvokeRuleInterface $rule
     * @param array $invokableClasses
     */
    protected function invokeAll(AutoInvokeRuleInterface $rule, array $invokableClasses)
    {
        foreach ($invokableClasses as $invokableClass) {
            call_user_func([$invokableClass, $rule->getInvokableMethodName()]);
        }
    }
}
