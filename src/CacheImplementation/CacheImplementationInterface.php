<?php

declare(strict_types=1);


namespace Csoft\AutoInvoker\CacheImplementation;


use Csoft\AutoInvoker\AutoInvokeRule\AutoInvokeRuleInterface;
use Csoft\AutoInvoker\ClassFinder\ClassFinderInterface;

interface CacheImplementationInterface extends ClassFinderInterface
{
    /**
     * Stores the matching class list.
     *
     * @param AutoInvokeRuleInterface $rule
     * @param array $matchingClasses
     *
     * @return void
     */
    public function storeMatchingClasses(AutoInvokeRuleInterface $rule, array $matchingClasses): bool;

    /**
     * Resets the cache.
     *
     * @return bool
     */
    public function resetCache(): bool;
}
