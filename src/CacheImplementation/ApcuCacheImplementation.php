<?php

declare(strict_types=1);


namespace Csoft\AutoInvoker\CacheImplementation;


use Csoft\AutoInvoker\AutoInvokeRule\AutoInvokeRuleInterface;
use Csoft\AutoInvoker\NotFoundException;
use ErrorException;

class ApcuCacheImplementation extends AbstractCacheImplementation
{
    /**
     * ApcuCacheImplementation constructor.
     *
     * @throws ErrorException
     */
    public function __construct()
    {
        if (function_exists('apcu_store') === false) {
            throw new ErrorException('The apcu extension is not installed on the system!');
        }
    }

    /**
     * @inheritDoc
     */
    public function storeMatchingClasses(AutoInvokeRuleInterface $rule, array $matchingClasses): bool
    {
        return apcu_store(
            $this->getCacheKey($rule),
            json_encode($matchingClasses)
        );
    }

    /**
     * @inheritDoc
     */
    public function getMatchingClasses(AutoInvokeRuleInterface $rule): array
    {
        $matchingClassesJson = apcu_fetch($this->getCacheKey($rule));
        if ($matchingClassesJson === false) {
            throw new NotFoundException(sprintf('The cache entry does not exist!'));
        }

        return json_decode(
            $matchingClassesJson,
            true
        );
    }

    /**
     * @inheritDoc
     */
    public function resetCache(): bool
    {
        return apcu_clear_cache();
    }
}
