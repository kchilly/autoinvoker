<?php

declare(strict_types=1);


namespace Csoft\AutoInvoker\CacheImplementation;


use Csoft\AutoInvoker\AutoInvokeRule\AutoInvokeRuleInterface;
use Csoft\AutoInvoker\NotFoundException;
use ErrorException;
use Symfony\Component\Filesystem\Filesystem;

class FileSystemCacheImplementation extends AbstractCacheImplementation
{
    /** @var string */
    private $path;

    /** @var Filesystem */
    private $fileSystem;

    /**
     * FileSystemCacheImplementation constructor.
     *
     * @param string $path
     *
     * @throws ErrorException
     */
    public function __construct(string $path)
    {
        $this->fileSystem = new Filesystem();
        if ($this->fileSystem->exists($path) === false) {
            $this->fileSystem->mkdir($path);
        }

        $this->path = $path;
    }

    /**
     * @inheritDoc
     */
    public function storeMatchingClasses(AutoInvokeRuleInterface $rule, array $matchingClasses): bool
    {
        return false !== file_put_contents(
                $this->getCacheFilePath($rule),
                json_encode($matchingClasses)
            );
    }

    /**
     * Returns the cache file path.
     *
     * @param AutoInvokeRuleInterface $rule
     *
     * @return string
     */
    public function getCacheFilePath(AutoInvokeRuleInterface $rule): string
    {
        return $this->path . '/' . $this->getCacheKey($rule) . '.json';
    }

    /**
     * @inheritDoc
     */
    public function getMatchingClasses(AutoInvokeRuleInterface $rule): array
    {
        $filePath = $this->getCacheFilePath($rule);
        if ($this->fileSystem->exists($filePath) === false) {
            throw new NotFoundException(sprintf('The %s cache file does not exist!', $filePath));
        }

        return json_decode(
            file_get_contents($filePath),
            true
        );
    }

    /**
     * @inheritDoc
     */
    public function resetCache(): bool
    {
        $this->fileSystem->remove($this->path);
        $this->fileSystem->mkdir($this->path);

        return true;
    }
}
