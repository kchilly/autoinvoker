<?php

declare(strict_types=1);


namespace Csoft\AutoInvoker\CacheImplementation;


use Csoft\AutoInvoker\AutoInvokeRule\AutoInvokeRuleInterface;

abstract class AbstractCacheImplementation implements CacheImplementationInterface
{
    /**
     * Generates the cache key string.
     *
     * @param AutoInvokeRuleInterface $rule
     *
     * @return string
     */
    protected function getCacheKey(AutoInvokeRuleInterface $rule): string
    {
        return hash('crc32b', get_class($rule) . '|' . implode('|', $rule->getSourcePaths()));
    }
}
