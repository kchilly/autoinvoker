<?php

declare(strict_types=1);


namespace Csoft\AutoInvoker\ClassFinder;


use Csoft\AutoInvoker\AutoInvokeRule\AutoInvokeRuleInterface;
use Csoft\AutoInvoker\NotFoundException;

interface ClassFinderInterface
{
    /**
     * Returns the matching class list.
     *
     * @param AutoInvokeRuleInterface $rule
     *
     * @return array
     *
     * @throws NotFoundException
     */
    public function getMatchingClasses(AutoInvokeRuleInterface $rule): array;
}
