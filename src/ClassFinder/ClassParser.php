<?php

declare(strict_types=1);


namespace Csoft\AutoInvoker\ClassFinder;


class ClassParser
{
    /**
     * Returns the namespace from the given content or empty string when there was no namespace found.
     *
     * @param string $content
     *
     * @return string
     */
    public static function getNameSpaceFromContent(string $content): string
    {
        if (preg_match('#namespace[ ]+(?<namespace>[a-z0-9\\\_]+)#i', $content, $matches) === 1) {
            return $matches['namespace'];
        }

        return '';
    }

    /**
     * Returns the class from the given content or empty array when there was no class found.
     *
     * @param string $content
     *
     * @return array
     */
    public static function getClassNameFromContent(string $content): array
    {
        if (preg_match_all('#class[ ]+(?<className>[a-z0-9_]+)#i', $content, $matches) > 0) {
            return $matches['className'];
        }

        return [];
    }
}
