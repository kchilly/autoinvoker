<?php

declare(strict_types=1);


namespace Csoft\AutoInvoker\AutoInvokeRule;


use Csoft\AutoInvoker\InvokableInterface\AutoRegisterInterface;

class AutoRegisterRule extends AbstractAutoInvokeRule
{
    /**
     * @inheritDoc
     */
    public function getInvokableInterface(): string
    {
        return AutoRegisterInterface::class;
    }

    /**
     * @inheritDoc
     */
    public function getInvokableMethodName(): string
    {
        return 'register';
    }
}
