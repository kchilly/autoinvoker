<?php

declare(strict_types=1);


namespace Csoft\AutoInvoker\AutoInvokeRule;


abstract class AbstractAutoInvokeRule implements AutoInvokeRuleInterface
{
    /** @var array */
    private $sourcePath;

    public function __construct(array $sourcePath)
    {
        $this->sourcePath = $sourcePath;
    }

    /**
     * @inheritDoc
     */
    public function getSourcePaths(): array
    {
        return $this->sourcePath;
    }
}
