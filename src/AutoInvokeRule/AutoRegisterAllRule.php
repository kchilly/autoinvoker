<?php

declare(strict_types=1);


namespace Csoft\AutoInvoker\AutoInvokeRule;


class AutoRegisterAllRule extends AbstractAutoInvokeRule
{
    /**
     * @inheritDoc
     */
    public function getInvokableInterface(): string
    {
        return '';
    }

    /**
     * @inheritDoc
     */
    public function getInvokableMethodName(): string
    {
        return 'register';
    }
}
