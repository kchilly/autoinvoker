<?php

declare(strict_types=1);


namespace Csoft\AutoInvoker\AutoInvokeRule;


interface AutoInvokeRuleInterface
{
    /**
     * Returns the invokable full namespaced class.
     *
     * @return string
     */
    public function getInvokableInterface(): string;

    /**
     * Returns the invokable static method of the class.
     *
     * @return string
     */
    public function getInvokableMethodName(): string;

    /**
     * Returns the source code paths for the class lookup.
     *
     * @return array
     */
    public function getSourcePaths(): array;
}
