<?php

declare(strict_types=1);


namespace Csoft\AutoInvoker\InvokableInterface;


interface AutoRegisterInterface
{
    /**
     * The invokable method.
     *
     * @return void
     */
    public static function register();
}
